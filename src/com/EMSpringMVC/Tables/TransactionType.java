package com.EMSpringMVC.Tables;

public class TransactionType {

	private int id;
	private String transactionType;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	@Override
	public String toString() {
		return "TransactionType [id=" + id + ", transactionType=" + transactionType + "]";
	}
	
}
