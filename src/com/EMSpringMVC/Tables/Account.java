package com.EMSpringMVC.Tables;

import javax.validation.constraints.Size;

public class Account {
	
	private int id;

	@Size(min=5, max=100)
	private String name;
	
	private float balance;
	
	private float openingBalance;
	
	public Account(){
		
	}
		
	public Account(String account, float balance, float openingBalance) {
		this.name = account;
		this.balance = balance;
		this.openingBalance = openingBalance;
	}
	
	public Account(int id, String account, float balance, float openingBalance) {
		this.id = id;
		this.name = account;
		this.balance = balance;
		this.openingBalance = openingBalance;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public float getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(float openingBalance) {
		this.openingBalance = openingBalance;
	}
	
	@Override
	public String toString() {
		return "Account [id=" + id + ", name=" + name + ", balance=" + balance + ", openingBalance="
				+ openingBalance + "]";
	}
	
	
}
