package com.EMSpringMVC.Tables;

public class Transaction {
	
	private int id;
	private int typeID;
	private float ammount;
	private int originAccountID;
	private int destinyAccountID;
	private int categoryID;
	private String date;
	private String notes;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTypeID() {
		return typeID;
	}
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}
	public float getAmmount() {
		return ammount;
	}
	public void setAmmount(float ammount) {
		this.ammount = ammount;
	}
	public int getOriginAccountID() {
		return originAccountID;
	}
	public void setOriginAccountID(int originAccountID) {
		this.originAccountID = originAccountID;
	}
	public int getDestinyAccountID() {
		return destinyAccountID;
	}
	public void setDestinyAccountID(int destinyAccountID) {
		this.destinyAccountID = destinyAccountID;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", typeID=" + typeID + ", ammount=" + ammount + ", originAccountID="
				+ originAccountID + ", destinyAccountID=" + destinyAccountID + ", categoryID=" + categoryID + ", date="
				+ date + ", notes=" + notes + "]";
	}
	
	

}
