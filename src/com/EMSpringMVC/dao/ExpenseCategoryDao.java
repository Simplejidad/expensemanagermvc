package com.EMSpringMVC.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.EMSpringMVC.Tables.ExpenseCategory;

@Component("ExpenseCategoryDao")
public class ExpenseCategoryDao {

	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}

	public List<ExpenseCategory> getExpenseCategories() {

		return jdbc.query("select * from Expense_Categories", new RowMapper<ExpenseCategory>() {

			public ExpenseCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
				ExpenseCategory expenseCategory = new ExpenseCategory();

				expenseCategory.setId(rs.getInt("id"));
				expenseCategory.setName(rs.getString("name"));

				return expenseCategory;
			}
		});
	}
	
	public ExpenseCategory getExpenseCategory(int id){
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		
		return jdbc.queryForObject("select * from Expense_Categories where id=:id",  params,  new RowMapper<ExpenseCategory>() {
			
			public ExpenseCategory mapRow(ResultSet rs, int rowNum)
				throws SQLException {
				ExpenseCategory expenseCategory = new ExpenseCategory();
				
				expenseCategory.setId(rs.getInt("id"));
				expenseCategory.setName(rs.getString("name"));
				
				return expenseCategory;
			}
		});
	}
	
}
