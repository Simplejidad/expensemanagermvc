package com.EMSpringMVC.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.EMSpringMVC.Tables.Account;

@Component("accountsDao")
public class AccountsDao {
	
	private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}
	
	public List<Account> getAccounts(){
		
		return jdbc.query("select * from Accounts", new RowMapper<Account>() {
			
			public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
				Account account = new Account();
				
				account.setId(rs.getInt("id"));
				account.setName(rs.getString("name"));
				account.setBalance(rs.getFloat("balance"));
				account.setOpeningBalance(rs.getFloat("opening_balance"));
				
				return account;
			}
		});
	}
	
	public Account getAccount(int id) {
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id",  id);
		
		return jdbc.queryForObject("select * from Accounts where id=:id",  params, new RowMapper<Account>() {
			
			public Account mapRow(ResultSet rs, int rowNum)
				throws SQLException {
				Account account = new Account();
				
				account.setId(rs.getInt("id"));
				account.setName(rs.getString("name"));
				account.setOpeningBalance(rs.getFloat("openingBalance"));
				account.setBalance(rs.getFloat("balance"));
				
				return account;
			}
		});
	}
	
	public boolean delete(int id){
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);
		
		return jdbc.update("delete from Accounts where id=:id", params) == 1;
		
	}
	
	public boolean create(Account account) {
		
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(account);
		
		return jdbc.update("insert into Accounts(Name, Balance, Opening_Balance) values (:name, :openingBalance, :openingBalance)", params) ==1;

	}
	
	public boolean update(Account account) {
		
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(account);
		
		return jdbc.update("update Accounts set Name=:name, Balance=:balance, Opening_Balance=:openingBalance where ID=:id", params) == 1;
	}
	
	@Transactional
	public int[] create(List<Account> accounts){
		
		SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(accounts.toArray());
		
		return jdbc.batchUpdate("insert into Accounts(Name, Balance, Opening_Balance) values (:name, :balance, :openingBalance)", params);
	}
	

}
