package com.EMSpringMVC.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.EMSpringMVC.Tables.Account;
import com.EMSpringMVC.dao.AccountsDao;

@Service("accountsService")
public class AccountsService {

	private AccountsDao accountsDao;

	@Autowired
	public void setAccountsDao(AccountsDao accountsDao) {
		this.accountsDao = accountsDao;
	}

	public List<Account> getCurrent() {
		return accountsDao.getAccounts();
	}

	public void create(Account account) {

		accountsDao.create(account);
	}

}
