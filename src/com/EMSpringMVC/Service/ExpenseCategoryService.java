package com.EMSpringMVC.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.EMSpringMVC.Tables.ExpenseCategory;
import com.EMSpringMVC.dao.ExpenseCategoryDao;

@Service("expenseCategoryService")
public class ExpenseCategoryService {
	
	private ExpenseCategoryDao expenseCategoryDao;
	
	@Autowired
	public void setExpenseCategoryDao(ExpenseCategoryDao expenseCategoryDao) {
		this.expenseCategoryDao = expenseCategoryDao;
	}
	
	public List<ExpenseCategory> getCurrent() {
		return expenseCategoryDao.getExpenseCategories();
	}

}
