package com.EMSpringMVC.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.EMSpringMVC.Service.ExpenseCategoryService;
import com.EMSpringMVC.Tables.ExpenseCategory;

@Controller
public class ExpenseCategoryController {
	
	private ExpenseCategoryService expenseCategoryService;
	
	@Autowired
	public void setExpenseCategoryService(ExpenseCategoryService expenseCategoryService) {
		this.expenseCategoryService = expenseCategoryService;
	}
	
	@RequestMapping("/expensecategories")
	public String showExpenseCategories(Model model){
		
		List<ExpenseCategory> expenseCategories = expenseCategoryService.getCurrent();
		
		model.addAttribute("expenseCategories", expenseCategories);
	
	return "expensecategories";
	
	}

}
