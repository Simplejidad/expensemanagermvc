package com.EMSpringMVC.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.EMSpringMVC.Service.AccountsService;
import com.EMSpringMVC.Tables.Account;

@Controller
public class AccountsController {

	private AccountsService accountsService;

	@Autowired
	public void setAccountService(AccountsService accountsService) {
		this.accountsService = accountsService;
	}

	@RequestMapping("/accounts")
	public String showAccounts(Model model) {

		List<Account> accounts = accountsService.getCurrent();

		model.addAttribute("accounts", accounts);

		return "accounts";
	}

	@RequestMapping("/createaccount")
	public String createAccount(Model model) {
		
		model.addAttribute("account", new Account());

		return "createaccount";
	}

	@RequestMapping(value = "/docreate", method = RequestMethod.POST)
	public String doCreate(Model model, @Valid Account account, BindingResult result) {

		if (result.hasErrors()) {
			
			return "createaccount";
		}
		
		accountsService.create(account);
		

		return "accountcreated";
	}
	
}
