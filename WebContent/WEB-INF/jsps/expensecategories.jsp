<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link href="${pageContext.request.contextPath}/static/main.css"
	rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Current Expense Categories.</title>
<body>

	<table class="accounts">
		<tr>
			<td>ID</td>
			<td>Name</td>
		</tr>
		<c:forEach var="expenseCategory" items="${expenseCategories}">
			<tr>
				<td><c:out value="${expenseCategory.id}"></c:out></td>
				<td><c:out value="${expenseCategory.name}"></c:out></td>

			</tr>
		</c:forEach>
	</table>